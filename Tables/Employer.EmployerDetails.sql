CREATE TABLE [Employer].[EmployerDetails]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UserId] [int] NOT NULL,
[CompanyName] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[EmployerDetails] ADD CONSTRAINT [PK_EmployerDetails] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Employer].[EmployerDetails] ADD CONSTRAINT [FK_EmployerDetails_Users] FOREIGN KEY ([UserId]) REFERENCES [Common].[Users] ([Id])
GO
