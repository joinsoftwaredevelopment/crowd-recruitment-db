CREATE TABLE [Common].[GenderTranslation]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[translation] [nvarchar] (50) COLLATE Arabic_100_CI_AI NULL,
[genderId] [int] NULL,
[languageId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [Common].[GenderTranslation] ADD CONSTRAINT [PK_GenderTranslation] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
