CREATE TABLE [Common].[LanguagesTranslation]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[languageId] [int] NOT NULL,
[name] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Common].[LanguagesTranslation] ADD CONSTRAINT [PK_LanguagesTranslation] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
