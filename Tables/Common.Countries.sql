CREATE TABLE [Common].[Countries]
(
[id] [int] NOT NULL,
[createdDate] [datetime] NOT NULL,
[iso3] [varchar] (3) COLLATE Arabic_100_CI_AI NOT NULL,
[iso2] [varchar] (2) COLLATE Arabic_100_CI_AI NOT NULL,
[phonecode] [varchar] (20) COLLATE Arabic_100_CI_AI NULL,
[currency] [nvarchar] (4) COLLATE Arabic_100_CI_AI NULL
) ON [PRIMARY]
GO
ALTER TABLE [Common].[Countries] ADD CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
