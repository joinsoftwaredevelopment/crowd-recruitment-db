CREATE TABLE [Employer].[MustHaveQualification]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UUID] [varchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[MustHaveQualification] ADD CONSTRAINT [PK_MustHaveQualification] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Employer].[MustHaveQualification] ADD CONSTRAINT [UQ__MustHave__65A475E69D625CD6] UNIQUE NONCLUSTERED  ([UUID]) ON [PRIMARY]
GO
