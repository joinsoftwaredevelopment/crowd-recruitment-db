CREATE TABLE [Job].[JobLocationDetails]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[JobId] [int] NOT NULL,
[SourcerCityLocationID] [int] NULL,
[SourcerCountryLocationID] [int] NULL,
[RemoteTypeId] [int] NULL,
[TimeZoneFromId] [int] NULL,
[TimeZoneToId] [int] NULL,
[Other] [nvarchar] (50) COLLATE Arabic_100_CI_AI NULL
) ON [PRIMARY]
GO
ALTER TABLE [Job].[JobLocationDetails] ADD CONSTRAINT [PK_JobLocationDetails] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Job].[JobLocationDetails] ADD CONSTRAINT [FK_JobLocationDetails_JobLocationDetails] FOREIGN KEY ([RemoteTypeId]) REFERENCES [Employer].[RemoteType] ([Id])
GO
ALTER TABLE [Job].[JobLocationDetails] ADD CONSTRAINT [FK_JobLocationDetails_JobLocationDetails1] FOREIGN KEY ([Id]) REFERENCES [Job].[JobLocationDetails] ([Id])
GO
