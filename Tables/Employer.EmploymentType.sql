CREATE TABLE [Employer].[EmploymentType]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UUID] [varchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[EmploymentType] ADD CONSTRAINT [PK_EmploymentType] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Employer].[EmploymentType] ADD CONSTRAINT [UQ__Employme__65A475E61FDDD1D0] UNIQUE NONCLUSTERED  ([UUID]) ON [PRIMARY]
GO
