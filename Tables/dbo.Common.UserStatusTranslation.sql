CREATE TABLE [dbo].[Common.UserStatusTranslation]
(
[id] [int] NOT NULL,
[userStatusId] [int] NOT NULL,
[translation] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[languageId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Common.UserStatusTranslation] ADD CONSTRAINT [PK_Common.UserStatusTranslation] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
