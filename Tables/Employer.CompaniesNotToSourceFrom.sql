CREATE TABLE [Employer].[CompaniesNotToSourceFrom]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UUID] [varchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[JobId] [int] NOT NULL,
[Text] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[CompaniesNotToSourceFrom] ADD CONSTRAINT [PK_CompaniesNotToSourceFrom] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Employer].[CompaniesNotToSourceFrom] ADD CONSTRAINT [UQ__Companie__65A475E6A8E5BBD9] UNIQUE NONCLUSTERED  ([UUID]) ON [PRIMARY]
GO
