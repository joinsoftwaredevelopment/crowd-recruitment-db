CREATE TABLE [Common].[RolesTranslation]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[RoleId] [int] NOT NULL,
[Translation] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[languageId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Common].[RolesTranslation] ADD CONSTRAINT [PK_RolesTranslation] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Common].[RolesTranslation] ADD CONSTRAINT [FK_RolesTranslation_RolesTranslation] FOREIGN KEY ([Id]) REFERENCES [Common].[RolesTranslation] ([Id])
GO
