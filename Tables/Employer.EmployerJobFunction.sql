CREATE TABLE [Employer].[EmployerJobFunction]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UUID] [varchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[JobId] [int] NOT NULL,
[JobFunctionId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[EmployerJobFunction] ADD CONSTRAINT [PK_EmplyerJobFunction] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Employer].[EmployerJobFunction] ADD CONSTRAINT [UQ__Employer__65A475E633129B97] UNIQUE NONCLUSTERED  ([UUID]) ON [PRIMARY]
GO
ALTER TABLE [Employer].[EmployerJobFunction] ADD CONSTRAINT [FK_JobFunction_Job] FOREIGN KEY ([JobFunctionId]) REFERENCES [Job].[JobFunction] ([Id])
GO
