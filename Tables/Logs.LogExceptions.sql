CREATE TABLE [Logs].[LogExceptions]
(
[id] [bigint] NOT NULL IDENTITY(1, 1),
[logDate] [datetime] NULL,
[assemblyName] [varchar] (100) COLLATE Arabic_100_CI_AI NULL,
[objectName] [varchar] (100) COLLATE Arabic_100_CI_AI NULL,
[methodName] [varchar] (100) COLLATE Arabic_100_CI_AI NULL,
[innerException] [varchar] (500) COLLATE Arabic_100_CI_AI NULL,
[errorMessage] [varchar] (500) COLLATE Arabic_100_CI_AI NULL
) ON [PRIMARY]
GO
ALTER TABLE [Logs].[LogExceptions] ADD CONSTRAINT [PK_Exceptions] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
