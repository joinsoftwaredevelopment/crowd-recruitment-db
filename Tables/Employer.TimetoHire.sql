CREATE TABLE [Employer].[TimetoHire]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UUID] [varchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[TimetoHire] ADD CONSTRAINT [PK_TimetoHire] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Employer].[TimetoHire] ADD CONSTRAINT [UQ__TimetoHi__65A475E6F0CA6786] UNIQUE NONCLUSTERED  ([UUID]) ON [PRIMARY]
GO
