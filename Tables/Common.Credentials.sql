CREATE TABLE [Common].[Credentials]
(
[Id] [tinyint] NOT NULL,
[smsApiAccountId] [varchar] (300) COLLATE Arabic_100_CI_AI NULL,
[smsApiTokenKey] [varchar] (300) COLLATE Arabic_100_CI_AI NULL,
[smsApiPhone] [varchar] (300) COLLATE Arabic_100_CI_AI NULL,
[notificationEmail] [varchar] (200) COLLATE Arabic_100_CI_AI NULL,
[notificationEmailPassword] [varchar] (200) COLLATE Arabic_100_CI_AI NULL,
[notificationEmailIncomingServer] [varchar] (200) COLLATE Arabic_100_CI_AI NULL,
[notificationEmailOutgoingServer] [varchar] (200) COLLATE Arabic_100_CI_AI NULL,
[notificationEmailPort] [varchar] (200) COLLATE Arabic_100_CI_AI NULL,
[jwtAccountId] [varchar] (100) COLLATE Arabic_100_CI_AI NULL,
[jwtTokenKey] [varchar] (500) COLLATE Arabic_100_CI_AI NULL,
[jwtSecretKey] [varchar] (500) COLLATE Arabic_100_CI_AI NULL,
[jwtAudience] [varchar] (500) COLLATE Arabic_100_CI_AI NULL,
[jwtIssuer] [varchar] (500) COLLATE Arabic_100_CI_AI NULL,
[jwtExpriationTime] [varchar] (500) COLLATE Arabic_100_CI_AI NULL,
[googleApiAccountId] [varchar] (100) COLLATE Arabic_100_CI_AI NULL,
[googleApiTokenKey] [varchar] (100) COLLATE Arabic_100_CI_AI NULL,
[facebookApiAccountId] [varchar] (100) COLLATE Arabic_100_CI_AI NULL,
[facebookApiTokenKey] [varchar] (100) COLLATE Arabic_100_CI_AI NULL
) ON [PRIMARY]
GO
ALTER TABLE [Common].[Credentials] ADD CONSTRAINT [PK_Credentials] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
