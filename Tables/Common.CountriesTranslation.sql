CREATE TABLE [Common].[CountriesTranslation]
(
[countryId] [int] NOT NULL,
[languageId] [int] NOT NULL,
[name] [varchar] (50) COLLATE Arabic_100_CI_AI NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Common].[CountriesTranslation] ADD CONSTRAINT [PK_CountriesDescription] PRIMARY KEY CLUSTERED  ([countryId], [languageId]) ON [PRIMARY]
GO
ALTER TABLE [Common].[CountriesTranslation] ADD CONSTRAINT [FK_CountriesDescription_Countries] FOREIGN KEY ([countryId]) REFERENCES [Common].[Countries] ([id])
GO
