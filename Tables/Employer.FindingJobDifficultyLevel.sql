CREATE TABLE [Employer].[FindingJobDifficultyLevel]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UUID] [varchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[FindingJobDifficultyLevel] ADD CONSTRAINT [PK_FindingJobDifficultyLevel] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Employer].[FindingJobDifficultyLevel] ADD CONSTRAINT [UQ__FindingJ__65A475E6A4A7FD62] UNIQUE NONCLUSTERED  ([UUID]) ON [PRIMARY]
GO
