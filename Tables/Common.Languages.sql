CREATE TABLE [Common].[Languages]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[name] [varchar] (50) COLLATE Arabic_100_CI_AI NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Common].[Languages] ADD CONSTRAINT [PK_Languages] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
