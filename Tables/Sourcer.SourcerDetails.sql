CREATE TABLE [Sourcer].[SourcerDetails]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UserId] [int] NOT NULL,
[ExperienceTypeId] [int] NULL,
[JobTitle] [nvarchar] (50) COLLATE Arabic_100_CI_AI NULL,
[HasAccessToDifferentWebsite] [bit] NULL,
[IndustryId] [int] NULL,
[CvName] [nvarchar] (50) COLLATE Arabic_100_CI_AI NULL,
[LinkedInProfile] [nvarchar] (100) COLLATE Arabic_100_CI_AI NULL
) ON [PRIMARY]
GO
ALTER TABLE [Sourcer].[SourcerDetails] ADD CONSTRAINT [PK_SourcerDetails] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Sourcer].[SourcerDetails] ADD CONSTRAINT [FK_SourcerDetails_Users] FOREIGN KEY ([UserId]) REFERENCES [Common].[Users] ([Id])
GO
