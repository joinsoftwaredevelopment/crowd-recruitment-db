CREATE TABLE [Common].[Interviewers]
(
[Id] [int] NOT NULL,
[InterviewerName] [nvarchar] (200) COLLATE Arabic_100_CI_AI NOT NULL,
[InterviewerImage] [nvarchar] (200) COLLATE Arabic_100_CI_AI NOT NULL,
[zoomLink] [nvarchar] (max) COLLATE Arabic_100_CI_AI NULL,
[email] [nvarchar] (200) COLLATE Arabic_100_CI_AI NULL
) ON [PRIMARY]
GO
ALTER TABLE [Common].[Interviewers] ADD CONSTRAINT [PK_Interviewers] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
