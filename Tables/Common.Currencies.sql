CREATE TABLE [Common].[Currencies]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[createdDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Common].[Currencies] ADD CONSTRAINT [PK_Currencies] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
