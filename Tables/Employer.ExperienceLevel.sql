CREATE TABLE [Employer].[ExperienceLevel]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UUID] [varchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[ExperienceLevel] ADD CONSTRAINT [PK_ExperienceLevel] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Employer].[ExperienceLevel] ADD CONSTRAINT [UQ__Experien__65A475E6FFFCC814] UNIQUE NONCLUSTERED  ([UUID]) ON [PRIMARY]
GO
