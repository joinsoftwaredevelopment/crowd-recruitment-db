CREATE TABLE [dbo].[Common.UserStatus]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[createdDate] [date] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Common.UserStatus] ADD CONSTRAINT [PK_Common.UserStatus] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
