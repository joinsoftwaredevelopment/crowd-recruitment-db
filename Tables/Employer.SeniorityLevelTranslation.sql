CREATE TABLE [Employer].[SeniorityLevelTranslation]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[SeniorityLevelId] [int] NOT NULL,
[Translation] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[languageId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[SeniorityLevelTranslation] ADD CONSTRAINT [PK_SeniorityLevelTranslation] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
