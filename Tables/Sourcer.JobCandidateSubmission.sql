CREATE TABLE [Sourcer].[JobCandidateSubmission]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UUID] [varchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[JobId] [int] NOT NULL,
[candidateName] [nvarchar] (150) COLLATE Arabic_100_CI_AI NULL,
[CV_Name] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[SubmissionDate] [datetime] NOT NULL,
[SourcerId] [int] NOT NULL,
[CandidateSubmissionStatusId] [int] NOT NULL,
[linkedInProfile] [nvarchar] (300) COLLATE Arabic_100_CI_AI NULL,
[locationId] [int] NULL,
[IsDeleted] [bit] NOT NULL,
[commission] [decimal] (10, 3) NULL
) ON [PRIMARY]
GO
ALTER TABLE [Sourcer].[JobCandidateSubmission] ADD CONSTRAINT [PK_JobCandidateSubmission] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Sourcer].[JobCandidateSubmission] ADD CONSTRAINT [UQ__JobCandi__65A475E6BD9808BB] UNIQUE NONCLUSTERED  ([UUID]) ON [PRIMARY]
GO
