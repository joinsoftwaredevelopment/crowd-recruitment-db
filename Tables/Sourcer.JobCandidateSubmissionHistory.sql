CREATE TABLE [Sourcer].[JobCandidateSubmissionHistory]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[CandidateSubmissionStatusId] [int] NOT NULL,
[Comment] [nvarchar] (50) COLLATE Arabic_100_CI_AI NULL,
[JobCandidatesubmissionId] [int] NOT NULL,
[createDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Sourcer].[JobCandidateSubmissionHistory] ADD CONSTRAINT [PK_JobCandidateSubmissionHistory] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
