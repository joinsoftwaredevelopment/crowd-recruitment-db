CREATE TABLE [Employer].[EmployerNiceHaveQualification]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UUID] [varchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[JobId] [int] NOT NULL,
[Text] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[EmployerNiceHaveQualification] ADD CONSTRAINT [PK_EmployerNiceHaveQualification] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Employer].[EmployerNiceHaveQualification] ADD CONSTRAINT [UQ__Employer__65A475E63C33C567] UNIQUE NONCLUSTERED  ([UUID]) ON [PRIMARY]
GO
