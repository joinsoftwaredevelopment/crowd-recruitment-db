CREATE TABLE [Employer].[FindingJobDifficultyLevelTranslation]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[FindingJobDifficultyLevelId] [int] NOT NULL,
[Translation] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[languageId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[FindingJobDifficultyLevelTranslation] ADD CONSTRAINT [PK_FindingJobDifficultyLevelTranslation] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
