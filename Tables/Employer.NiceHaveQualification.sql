CREATE TABLE [Employer].[NiceHaveQualification]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UUID] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[NiceHaveQualification] ADD CONSTRAINT [PK_NiceHaveQualification] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
