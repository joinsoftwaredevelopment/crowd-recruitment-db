CREATE TABLE [Employer].[EmployerMustHaveQualification]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UUID] [varchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[JobId] [int] NOT NULL,
[Text] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[EmployerMustHaveQualification] ADD CONSTRAINT [PK_EmployerMustHaveQualification] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Employer].[EmployerMustHaveQualification] ADD CONSTRAINT [UQ__Employer__65A475E60C6C1AB4] UNIQUE NONCLUSTERED  ([UUID]) ON [PRIMARY]
GO
