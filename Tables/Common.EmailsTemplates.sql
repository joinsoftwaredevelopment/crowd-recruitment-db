CREATE TABLE [Common].[EmailsTemplates]
(
[id] [int] NOT NULL,
[emailTitleEN] [nvarchar] (200) COLLATE Arabic_100_CI_AI NOT NULL,
[emailTitleAr] [nvarchar] (200) COLLATE Arabic_100_CI_AI NOT NULL,
[emailbodyEn] [nvarchar] (max) COLLATE Arabic_100_CI_AI NOT NULL,
[emailbodyAr] [nvarchar] (max) COLLATE Arabic_100_CI_AI NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Common].[EmailsTemplates] ADD CONSTRAINT [PK_EmailsTemplates] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
