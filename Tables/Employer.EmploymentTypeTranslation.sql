CREATE TABLE [Employer].[EmploymentTypeTranslation]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[EmploymentTypeId] [int] NOT NULL,
[Translation] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[languageId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[EmploymentTypeTranslation] ADD CONSTRAINT [PK_EmploymentTypeTranslation] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
