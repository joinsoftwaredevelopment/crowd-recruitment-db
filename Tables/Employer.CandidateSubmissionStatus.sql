CREATE TABLE [Employer].[CandidateSubmissionStatus]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UUID] [varchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[CandidateSubmissionStatus] ADD CONSTRAINT [PK_CandidateSubmissionStatus] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Employer].[CandidateSubmissionStatus] ADD CONSTRAINT [UQ__Candidat__65A475E69BA1FE46] UNIQUE NONCLUSTERED  ([UUID]) ON [PRIMARY]
GO
