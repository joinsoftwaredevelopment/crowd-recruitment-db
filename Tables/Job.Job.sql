CREATE TABLE [Job].[Job]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UUID] [varchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[EmployerId] [int] NOT NULL,
[JobTitle] [nvarchar] (50) COLLATE Arabic_100_CI_AI NULL,
[JobDescription] [nvarchar] (max) COLLATE Arabic_100_CI_AI NULL,
[JobDate] [datetime] NULL,
[SeniorityLevelId] [int] NULL,
[EmployerTypeId] [int] NULL,
[JobNationalityId] [int] NULL,
[ExperienceLevelId] [int] NULL,
[JobStatusId] [int] NULL,
[PeopleToHireCount] [int] NULL,
[IsContinuousHire] [bit] NULL,
[TimetoHireId] [int] NULL,
[NumberOfComingDays] [int] NULL,
[SourcingOusideYourCompanyId] [int] NULL,
[FindingJobDifficultyLevelId] [int] NULL,
[FirstMailSubject] [nvarchar] (50) COLLATE Arabic_100_CI_AI NULL,
[FirstMailDescription] [nvarchar] (1000) COLLATE Arabic_100_CI_AI NULL,
[FollowUp1Days] [nchar] (10) COLLATE Arabic_100_CI_AI NULL,
[Followup2Days] [nchar] (10) COLLATE Arabic_100_CI_AI NULL,
[IsDeleted] [bit] NOT NULL,
[JobLocationId] [int] NOT NULL,
[OfficeLocationId] [int] NULL,
[IsActive] [bit] NOT NULL,
[mustHaveQualification] [nvarchar] (500) COLLATE Arabic_100_CI_AI NULL,
[niceToHaveQualification] [nvarchar] (500) COLLATE Arabic_100_CI_AI NULL,
[companiesNotToSourceFrom] [nvarchar] (500) COLLATE Arabic_100_CI_AI NULL,
[HiringNeeds] [nvarchar] (50) COLLATE Arabic_100_CI_AI NULL,
[whenYouNeedToHire] [nvarchar] (50) COLLATE Arabic_100_CI_AI NULL,
[sourcingOutsideOfJoin] [bit] NULL,
[industryId] [int] NULL,
[commission] [decimal] (10, 3) NULL,
[reason] [nvarchar] (500) COLLATE Arabic_100_CI_AI NULL,
[noOfSubmissions] [int] NULL,
[requirements] [nvarchar] (1000) COLLATE Arabic_100_CI_AI NULL
) ON [PRIMARY]
GO
ALTER TABLE [Job].[Job] ADD CONSTRAINT [PK_Job] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Job].[Job] ADD CONSTRAINT [UQ__Job__65A475E6C4103BAF] UNIQUE NONCLUSTERED  ([UUID]) ON [PRIMARY]
GO
ALTER TABLE [Job].[Job] ADD CONSTRAINT [FK_Job_Countries] FOREIGN KEY ([JobNationalityId]) REFERENCES [Common].[Countries] ([id])
GO
ALTER TABLE [Job].[Job] ADD CONSTRAINT [FK_Job_EmploymentType] FOREIGN KEY ([EmployerTypeId]) REFERENCES [Employer].[EmploymentType] ([Id])
GO
ALTER TABLE [Job].[Job] ADD CONSTRAINT [FK_Job_ExperienceLevel] FOREIGN KEY ([ExperienceLevelId]) REFERENCES [Employer].[ExperienceLevel] ([Id])
GO
ALTER TABLE [Job].[Job] ADD CONSTRAINT [FK_Job_FindingJobDifficultyLevel] FOREIGN KEY ([FindingJobDifficultyLevelId]) REFERENCES [Employer].[FindingJobDifficultyLevel] ([Id])
GO
ALTER TABLE [Job].[Job] ADD CONSTRAINT [FK_Job_JobLocation] FOREIGN KEY ([JobLocationId]) REFERENCES [Job].[JobLocation] ([Id])
GO
ALTER TABLE [Job].[Job] ADD CONSTRAINT [FK_Job_JobStatus] FOREIGN KEY ([JobStatusId]) REFERENCES [Job].[JobStatus] ([Id])
GO
ALTER TABLE [Job].[Job] ADD CONSTRAINT [FK_Job_SeniorityLevel] FOREIGN KEY ([SeniorityLevelId]) REFERENCES [Employer].[SeniorityLevel] ([Id])
GO
ALTER TABLE [Job].[Job] ADD CONSTRAINT [FK_Job_Users] FOREIGN KEY ([EmployerId]) REFERENCES [Common].[Users] ([Id])
GO
