CREATE TABLE [Common].[Gender]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[createdDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [Common].[Gender] ADD CONSTRAINT [PK_Gender] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
