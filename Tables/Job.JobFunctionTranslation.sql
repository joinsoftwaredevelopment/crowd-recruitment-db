CREATE TABLE [Job].[JobFunctionTranslation]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[JobFunctionId] [int] NOT NULL,
[Translation] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[languageId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Job].[JobFunctionTranslation] ADD CONSTRAINT [PK_JobFunctionTranslation] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
