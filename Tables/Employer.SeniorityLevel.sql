CREATE TABLE [Employer].[SeniorityLevel]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UUID] [varchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[SeniorityLevel] ADD CONSTRAINT [PK_SeniorityLevel] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Employer].[SeniorityLevel] ADD CONSTRAINT [UQ__Seniorit__65A475E6324A735D] UNIQUE NONCLUSTERED  ([UUID]) ON [PRIMARY]
GO
