CREATE TABLE [Common].[Users]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UUID] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[FirstName] [varchar] (30) COLLATE Arabic_100_CI_AI NOT NULL,
[LastName] [varchar] (30) COLLATE Arabic_100_CI_AI NOT NULL,
[PhoneNumber] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[Email] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[IsEmailVerified] [bit] NULL,
[Password] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[RoleId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[UpdatedDate] [datetime] NULL,
[Active] [bit] NOT NULL,
[Accepted] [bit] NULL,
[MeetingDate] [datetime] NULL,
[MeetingPeriodId] [int] NULL,
[RegistrationStep] [int] NULL,
[InterviewerId] [int] NULL,
[meetingTimeId] [int] NULL,
[timeZoneId] [int] NULL,
[userStatusId] [int] NULL,
[rejectedReason] [nvarchar] (500) COLLATE Arabic_100_CI_AI NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [Common].[InsertSourcer]
   ON  [Common].[Users]
   AFTER INSERT
AS 
BEGIN

	SET NOCOUNT ON;

	Declare @userId int = (Select Id from inserted)
    Declare @roleId int = (Select RoleId from inserted)

	IF(@roleId = 3)
	    Insert INTO Sourcer.SourcerDetails (UserId) Values (@userId)

END
GO
ALTER TABLE [Common].[Users] ADD CONSTRAINT [PK_Common] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Common].[Users] ADD CONSTRAINT [UQ__Users__65A475E6ACDE64C5] UNIQUE NONCLUSTERED  ([UUID]) ON [PRIMARY]
GO
