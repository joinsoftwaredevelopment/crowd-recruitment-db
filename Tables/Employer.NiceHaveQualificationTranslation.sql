CREATE TABLE [Employer].[NiceHaveQualificationTranslation]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[NiceHaveQualificationId] [int] NOT NULL,
[Translation] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[languageId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[NiceHaveQualificationTranslation] ADD CONSTRAINT [PK_NiceHaveQualificationTranslation] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
