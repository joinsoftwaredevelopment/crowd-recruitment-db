CREATE TABLE [Employer].[CandidateSubmissionStatusTranslation]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[CandidateSubmissionStatusId] [int] NOT NULL,
[Translation] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[languageId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[CandidateSubmissionStatusTranslation] ADD CONSTRAINT [PK_CandidateSubmissionStatusTranslation] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Employer].[CandidateSubmissionStatusTranslation] ADD CONSTRAINT [FK_CandidateSubmissionStatusTranslation_CandidateSubmissionStatus] FOREIGN KEY ([CandidateSubmissionStatusId]) REFERENCES [Employer].[CandidateSubmissionStatus] ([Id])
GO
