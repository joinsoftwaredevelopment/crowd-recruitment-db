CREATE TABLE [Job].[JobStatusTranslation]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[JobStatusId] [int] NOT NULL,
[Translation] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[languageId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Job].[JobStatusTranslation] ADD CONSTRAINT [PK_JobStatusTranslation] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
