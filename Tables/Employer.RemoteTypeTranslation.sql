CREATE TABLE [Employer].[RemoteTypeTranslation]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[RemoteTypeId] [int] NOT NULL,
[Translation] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[languageId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[RemoteTypeTranslation] ADD CONSTRAINT [PK_RemoteTypeTranslation] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
