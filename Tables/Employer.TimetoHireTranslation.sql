CREATE TABLE [Employer].[TimetoHireTranslation]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[TimetoHireId] [int] NOT NULL,
[Translation] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[languageId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[TimetoHireTranslation] ADD CONSTRAINT [PK_TimetoHireTranslation] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
