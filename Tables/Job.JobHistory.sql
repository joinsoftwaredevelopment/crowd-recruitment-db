CREATE TABLE [Job].[JobHistory]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[JobStatusId] [int] NOT NULL,
[Comment] [nvarchar] (50) COLLATE Arabic_100_CI_AI NULL,
[JobId] [int] NOT NULL,
[createDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Job].[JobHistory] ADD CONSTRAINT [PK_JobHistory] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
