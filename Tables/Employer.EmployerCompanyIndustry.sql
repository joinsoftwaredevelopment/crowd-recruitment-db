CREATE TABLE [Employer].[EmployerCompanyIndustry]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UUID] [varchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[JobId] [int] NOT NULL,
[CompanyIndustryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[EmployerCompanyIndustry] ADD CONSTRAINT [PK_EmployerCompanyIndustry] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Employer].[EmployerCompanyIndustry] ADD CONSTRAINT [UQ__Employer__65A475E6DB8C2FDC] UNIQUE NONCLUSTERED  ([UUID]) ON [PRIMARY]
GO
