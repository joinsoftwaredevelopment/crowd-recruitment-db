CREATE TABLE [Employer].[ExperienceLevelTranslation]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ExperienceLevelId] [int] NOT NULL,
[Translation] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[languageId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[ExperienceLevelTranslation] ADD CONSTRAINT [PK_ExperienceLevelTranslation] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
