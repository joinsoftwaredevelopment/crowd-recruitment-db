CREATE TABLE [Job].[JobFunction]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UUID] [varchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Job].[JobFunction] ADD CONSTRAINT [PK_JobFunction] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Job].[JobFunction] ADD CONSTRAINT [UQ__JobFunct__65A475E6865A7D32] UNIQUE NONCLUSTERED  ([UUID]) ON [PRIMARY]
GO
