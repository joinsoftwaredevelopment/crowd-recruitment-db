CREATE TABLE [Common].[CurrencyTranslation]
(
[currencyId] [int] NOT NULL,
[languageId] [int] NOT NULL,
[name] [varchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[shortName] [varchar] (5) COLLATE Arabic_100_CI_AI NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Common].[CurrencyTranslation] ADD CONSTRAINT [PK_CurrencyDescription] PRIMARY KEY CLUSTERED  ([currencyId], [languageId]) ON [PRIMARY]
GO
ALTER TABLE [Common].[CurrencyTranslation] ADD CONSTRAINT [FK_CurrencyTranslation_Currencies] FOREIGN KEY ([currencyId]) REFERENCES [Common].[Currencies] ([id])
GO
