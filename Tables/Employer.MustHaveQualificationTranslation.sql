CREATE TABLE [Employer].[MustHaveQualificationTranslation]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[MustHaveQualificationId] [int] NOT NULL,
[Translation] [nvarchar] (50) COLLATE Arabic_100_CI_AI NOT NULL,
[languageId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Employer].[MustHaveQualificationTranslation] ADD CONSTRAINT [PK_MustHaveQualificationTranslation] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
