SET IDENTITY_INSERT [Employer].[CompanyIndustryTranslation] ON
INSERT INTO [Employer].[CompanyIndustryTranslation] ([Id], [CompanyIndustryId], [Translation], [languageId]) VALUES (1, 1, N'Business', 2)
INSERT INTO [Employer].[CompanyIndustryTranslation] ([Id], [CompanyIndustryId], [Translation], [languageId]) VALUES (2, 1, N'أعمال', 1)
INSERT INTO [Employer].[CompanyIndustryTranslation] ([Id], [CompanyIndustryId], [Translation], [languageId]) VALUES (3, 2, N'Tech', 2)
INSERT INTO [Employer].[CompanyIndustryTranslation] ([Id], [CompanyIndustryId], [Translation], [languageId]) VALUES (4, 2, N'برمجة', 1)
INSERT INTO [Employer].[CompanyIndustryTranslation] ([Id], [CompanyIndustryId], [Translation], [languageId]) VALUES (5, 3, N'Health care', 2)
INSERT INTO [Employer].[CompanyIndustryTranslation] ([Id], [CompanyIndustryId], [Translation], [languageId]) VALUES (7, 3, N'الصحية', 1)
INSERT INTO [Employer].[CompanyIndustryTranslation] ([Id], [CompanyIndustryId], [Translation], [languageId]) VALUES (8, 4, N'Admin & Sales', 2)
INSERT INTO [Employer].[CompanyIndustryTranslation] ([Id], [CompanyIndustryId], [Translation], [languageId]) VALUES (9, 4, N'أدمن & مبيعات', 1)
SET IDENTITY_INSERT [Employer].[CompanyIndustryTranslation] OFF
