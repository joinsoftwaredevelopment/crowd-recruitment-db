SET IDENTITY_INSERT [Employer].[RemoteTypeTranslation] ON
INSERT INTO [Employer].[RemoteTypeTranslation] ([Id], [RemoteTypeId], [Translation], [languageId]) VALUES (1, 1, N'Anywhere', 2)
INSERT INTO [Employer].[RemoteTypeTranslation] ([Id], [RemoteTypeId], [Translation], [languageId]) VALUES (2, 1, N'فى أى مكان', 1)
INSERT INTO [Employer].[RemoteTypeTranslation] ([Id], [RemoteTypeId], [Translation], [languageId]) VALUES (3, 2, N'Specific Location', 2)
INSERT INTO [Employer].[RemoteTypeTranslation] ([Id], [RemoteTypeId], [Translation], [languageId]) VALUES (4, 2, N'مكان محدد', 1)
INSERT INTO [Employer].[RemoteTypeTranslation] ([Id], [RemoteTypeId], [Translation], [languageId]) VALUES (5, 3, N'Specific time zones', 2)
INSERT INTO [Employer].[RemoteTypeTranslation] ([Id], [RemoteTypeId], [Translation], [languageId]) VALUES (6, 3, N'منطقة زمنية محددة', 1)
INSERT INTO [Employer].[RemoteTypeTranslation] ([Id], [RemoteTypeId], [Translation], [languageId]) VALUES (7, 4, N'Other', 2)
INSERT INTO [Employer].[RemoteTypeTranslation] ([Id], [RemoteTypeId], [Translation], [languageId]) VALUES (8, 4, N'أخرى', 1)
SET IDENTITY_INSERT [Employer].[RemoteTypeTranslation] OFF
