SET IDENTITY_INSERT [Employer].[ExperienceLevelTranslation] ON
INSERT INTO [Employer].[ExperienceLevelTranslation] ([Id], [ExperienceLevelId], [Translation], [languageId]) VALUES (1001, 4, N'10 +', 2)
INSERT INTO [Employer].[ExperienceLevelTranslation] ([Id], [ExperienceLevelId], [Translation], [languageId]) VALUES (1003, 4, N'10 +', 1)
SET IDENTITY_INSERT [Employer].[ExperienceLevelTranslation] OFF
SET IDENTITY_INSERT [Employer].[ExperienceLevelTranslation] ON
INSERT INTO [Employer].[ExperienceLevelTranslation] ([Id], [ExperienceLevelId], [Translation], [languageId]) VALUES (1, 1, N'0 - 2', 2)
INSERT INTO [Employer].[ExperienceLevelTranslation] ([Id], [ExperienceLevelId], [Translation], [languageId]) VALUES (2, 1, N'0 - 2', 1)
INSERT INTO [Employer].[ExperienceLevelTranslation] ([Id], [ExperienceLevelId], [Translation], [languageId]) VALUES (3, 2, N'3 - 5', 2)
INSERT INTO [Employer].[ExperienceLevelTranslation] ([Id], [ExperienceLevelId], [Translation], [languageId]) VALUES (4, 2, N'3 - 5', 1)
INSERT INTO [Employer].[ExperienceLevelTranslation] ([Id], [ExperienceLevelId], [Translation], [languageId]) VALUES (5, 3, N'6 - 10', 2)
INSERT INTO [Employer].[ExperienceLevelTranslation] ([Id], [ExperienceLevelId], [Translation], [languageId]) VALUES (6, 3, N'6 - 10', 1)
SET IDENTITY_INSERT [Employer].[ExperienceLevelTranslation] OFF
