SET IDENTITY_INSERT [Employer].[TimetoHireTranslation] ON
INSERT INTO [Employer].[TimetoHireTranslation] ([Id], [TimetoHireId], [Translation], [languageId]) VALUES (1, 1, N'As Soon as Possible', 2)
INSERT INTO [Employer].[TimetoHireTranslation] ([Id], [TimetoHireId], [Translation], [languageId]) VALUES (2, 1, N'فى أقرب وقت ممكن', 1)
INSERT INTO [Employer].[TimetoHireTranslation] ([Id], [TimetoHireId], [Translation], [languageId]) VALUES (3, 2, N'In the coming days', 2)
INSERT INTO [Employer].[TimetoHireTranslation] ([Id], [TimetoHireId], [Translation], [languageId]) VALUES (4, 2, N'فى الأيام القادمة', 1)
INSERT INTO [Employer].[TimetoHireTranslation] ([Id], [TimetoHireId], [Translation], [languageId]) VALUES (5, 3, N'No deadline , just pipelinling', 2)
INSERT INTO [Employer].[TimetoHireTranslation] ([Id], [TimetoHireId], [Translation], [languageId]) VALUES (6, 3, N'لا يوجد موعد نهائى', 1)
SET IDENTITY_INSERT [Employer].[TimetoHireTranslation] OFF
