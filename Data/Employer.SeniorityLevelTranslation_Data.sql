SET IDENTITY_INSERT [Employer].[SeniorityLevelTranslation] ON
INSERT INTO [Employer].[SeniorityLevelTranslation] ([Id], [SeniorityLevelId], [Translation], [languageId]) VALUES (1, 1, N'Student / Intern', 2)
INSERT INTO [Employer].[SeniorityLevelTranslation] ([Id], [SeniorityLevelId], [Translation], [languageId]) VALUES (2, 1, N'طالب /  فترة تدريب', 1)
SET IDENTITY_INSERT [Employer].[SeniorityLevelTranslation] OFF
SET IDENTITY_INSERT [Employer].[SeniorityLevelTranslation] ON
INSERT INTO [Employer].[SeniorityLevelTranslation] ([Id], [SeniorityLevelId], [Translation], [languageId]) VALUES (3, 2, N'Entry Level', 2)
INSERT INTO [Employer].[SeniorityLevelTranslation] ([Id], [SeniorityLevelId], [Translation], [languageId]) VALUES (4, 2, N'مبتدأ', 1)
INSERT INTO [Employer].[SeniorityLevelTranslation] ([Id], [SeniorityLevelId], [Translation], [languageId]) VALUES (5, 3, N'Associate', 2)
INSERT INTO [Employer].[SeniorityLevelTranslation] ([Id], [SeniorityLevelId], [Translation], [languageId]) VALUES (6, 3, N'مساعد', 1)
INSERT INTO [Employer].[SeniorityLevelTranslation] ([Id], [SeniorityLevelId], [Translation], [languageId]) VALUES (7, 4, N'Mid-senior Level', 2)
INSERT INTO [Employer].[SeniorityLevelTranslation] ([Id], [SeniorityLevelId], [Translation], [languageId]) VALUES (8, 4, N'أعلى من المتوسط', 1)
INSERT INTO [Employer].[SeniorityLevelTranslation] ([Id], [SeniorityLevelId], [Translation], [languageId]) VALUES (9, 5, N'Director', 2)
INSERT INTO [Employer].[SeniorityLevelTranslation] ([Id], [SeniorityLevelId], [Translation], [languageId]) VALUES (10, 5, N'مدير', 1)
INSERT INTO [Employer].[SeniorityLevelTranslation] ([Id], [SeniorityLevelId], [Translation], [languageId]) VALUES (11, 6, N'Executive', 2)
INSERT INTO [Employer].[SeniorityLevelTranslation] ([Id], [SeniorityLevelId], [Translation], [languageId]) VALUES (12, 6, N'مدير تنفيذى', 1)
INSERT INTO [Employer].[SeniorityLevelTranslation] ([Id], [SeniorityLevelId], [Translation], [languageId]) VALUES (13, 7, N'Not Applicable', 2)
INSERT INTO [Employer].[SeniorityLevelTranslation] ([Id], [SeniorityLevelId], [Translation], [languageId]) VALUES (14, 7, N'غير مناسب', 1)
SET IDENTITY_INSERT [Employer].[SeniorityLevelTranslation] OFF
