SET IDENTITY_INSERT [Employer].[SeniorityLevel] ON
INSERT INTO [Employer].[SeniorityLevel] ([Id], [UUID], [CreatedDate]) VALUES (1, N'1', '2020-12-24 00:00:00.000')
INSERT INTO [Employer].[SeniorityLevel] ([Id], [UUID], [CreatedDate]) VALUES (2, N'2', '2020-12-24 00:00:00.000')
INSERT INTO [Employer].[SeniorityLevel] ([Id], [UUID], [CreatedDate]) VALUES (3, N'3', '2020-12-24 00:00:00.000')
INSERT INTO [Employer].[SeniorityLevel] ([Id], [UUID], [CreatedDate]) VALUES (4, N'4', '2020-12-24 00:00:00.000')
INSERT INTO [Employer].[SeniorityLevel] ([Id], [UUID], [CreatedDate]) VALUES (5, N'5', '2020-12-24 00:00:00.000')
INSERT INTO [Employer].[SeniorityLevel] ([Id], [UUID], [CreatedDate]) VALUES (6, N'6', '2020-12-24 00:00:00.000')
INSERT INTO [Employer].[SeniorityLevel] ([Id], [UUID], [CreatedDate]) VALUES (7, N'7', '2020-12-24 00:00:00.000')
SET IDENTITY_INSERT [Employer].[SeniorityLevel] OFF
