SET IDENTITY_INSERT [Employer].[CandidateSubmissionStatusTranslation] ON
INSERT INTO [Employer].[CandidateSubmissionStatusTranslation] ([Id], [CandidateSubmissionStatusId], [Translation], [languageId]) VALUES (1, 1, N'Draft', 2)
INSERT INTO [Employer].[CandidateSubmissionStatusTranslation] ([Id], [CandidateSubmissionStatusId], [Translation], [languageId]) VALUES (2, 1, N'درافت', 1)
INSERT INTO [Employer].[CandidateSubmissionStatusTranslation] ([Id], [CandidateSubmissionStatusId], [Translation], [languageId]) VALUES (3, 2, N'Submitted', 2)
INSERT INTO [Employer].[CandidateSubmissionStatusTranslation] ([Id], [CandidateSubmissionStatusId], [Translation], [languageId]) VALUES (4, 2, N'مرسل', 1)
INSERT INTO [Employer].[CandidateSubmissionStatusTranslation] ([Id], [CandidateSubmissionStatusId], [Translation], [languageId]) VALUES (5, 3, N'Approved', 2)
INSERT INTO [Employer].[CandidateSubmissionStatusTranslation] ([Id], [CandidateSubmissionStatusId], [Translation], [languageId]) VALUES (6, 3, N'تم الموافقة عليه', 1)
INSERT INTO [Employer].[CandidateSubmissionStatusTranslation] ([Id], [CandidateSubmissionStatusId], [Translation], [languageId]) VALUES (7, 4, N'Disqualified', 2)
INSERT INTO [Employer].[CandidateSubmissionStatusTranslation] ([Id], [CandidateSubmissionStatusId], [Translation], [languageId]) VALUES (8, 4, N'غير مؤهل', 1)
SET IDENTITY_INSERT [Employer].[CandidateSubmissionStatusTranslation] OFF
