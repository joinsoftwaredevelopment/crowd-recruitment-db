SET IDENTITY_INSERT [Common].[RolesTranslation] ON
INSERT INTO [Common].[RolesTranslation] ([Id], [RoleId], [Translation], [languageId]) VALUES (1, 1, N'Admin', 2)
INSERT INTO [Common].[RolesTranslation] ([Id], [RoleId], [Translation], [languageId]) VALUES (2, 1, N'المسئول', 1)
INSERT INTO [Common].[RolesTranslation] ([Id], [RoleId], [Translation], [languageId]) VALUES (3, 2, N'Employer', 2)
INSERT INTO [Common].[RolesTranslation] ([Id], [RoleId], [Translation], [languageId]) VALUES (4, 2, N'صاحب العمل', 1)
INSERT INTO [Common].[RolesTranslation] ([Id], [RoleId], [Translation], [languageId]) VALUES (5, 3, N'Sourcer', 2)
INSERT INTO [Common].[RolesTranslation] ([Id], [RoleId], [Translation], [languageId]) VALUES (6, 3, N'المصدر', 1)
SET IDENTITY_INSERT [Common].[RolesTranslation] OFF
