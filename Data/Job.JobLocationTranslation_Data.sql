SET IDENTITY_INSERT [Job].[JobLocationTranslation] ON
INSERT INTO [Job].[JobLocationTranslation] ([Id], [JobLocationId], [Translation], [languageId]) VALUES (1, 1, N'On-site', 2)
INSERT INTO [Job].[JobLocationTranslation] ([Id], [JobLocationId], [Translation], [languageId]) VALUES (2, 1, N'فى مكتب الشركة', 1)
INSERT INTO [Job].[JobLocationTranslation] ([Id], [JobLocationId], [Translation], [languageId]) VALUES (3, 2, N'On-site and open to remote', 2)
INSERT INTO [Job].[JobLocationTranslation] ([Id], [JobLocationId], [Translation], [languageId]) VALUES (4, 2, N'فى مكتب الشركة و ممكن عن بعد', 1)
INSERT INTO [Job].[JobLocationTranslation] ([Id], [JobLocationId], [Translation], [languageId]) VALUES (5, 3, N'Remote Only', 2)
INSERT INTO [Job].[JobLocationTranslation] ([Id], [JobLocationId], [Translation], [languageId]) VALUES (6, 3, N'عن بعد فقط ', 1)
SET IDENTITY_INSERT [Job].[JobLocationTranslation] OFF
