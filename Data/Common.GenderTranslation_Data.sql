SET IDENTITY_INSERT [Common].[GenderTranslation] ON
INSERT INTO [Common].[GenderTranslation] ([id], [translation], [genderId], [languageId]) VALUES (1, N'ذكر', 1, 1)
INSERT INTO [Common].[GenderTranslation] ([id], [translation], [genderId], [languageId]) VALUES (2, N'Male', 1, 2)
INSERT INTO [Common].[GenderTranslation] ([id], [translation], [genderId], [languageId]) VALUES (3, N'أنثى', 2, 1)
INSERT INTO [Common].[GenderTranslation] ([id], [translation], [genderId], [languageId]) VALUES (4, N'Female', 2, 2)
SET IDENTITY_INSERT [Common].[GenderTranslation] OFF
