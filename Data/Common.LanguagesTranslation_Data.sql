SET IDENTITY_INSERT [Common].[LanguagesTranslation] ON
INSERT INTO [Common].[LanguagesTranslation] ([id], [languageId], [name]) VALUES (1, 2, N'Arabic')
INSERT INTO [Common].[LanguagesTranslation] ([id], [languageId], [name]) VALUES (2, 1, N'عربى')
INSERT INTO [Common].[LanguagesTranslation] ([id], [languageId], [name]) VALUES (3, 2, N'English')
INSERT INTO [Common].[LanguagesTranslation] ([id], [languageId], [name]) VALUES (4, 1, N'إنجليزى')
SET IDENTITY_INSERT [Common].[LanguagesTranslation] OFF
