SET IDENTITY_INSERT [Job].[JobStatusTranslation] ON
INSERT INTO [Job].[JobStatusTranslation] ([Id], [JobStatusId], [Translation], [languageId]) VALUES (1, 1, N'Draft', 2)
INSERT INTO [Job].[JobStatusTranslation] ([Id], [JobStatusId], [Translation], [languageId]) VALUES (2, 1, N'درافت', 1)
INSERT INTO [Job].[JobStatusTranslation] ([Id], [JobStatusId], [Translation], [languageId]) VALUES (5, 3, N'Approved', 2)
INSERT INTO [Job].[JobStatusTranslation] ([Id], [JobStatusId], [Translation], [languageId]) VALUES (6, 3, N'تم الموافقة عليه', 1)
INSERT INTO [Job].[JobStatusTranslation] ([Id], [JobStatusId], [Translation], [languageId]) VALUES (7, 4, N'Declined', 2)
INSERT INTO [Job].[JobStatusTranslation] ([Id], [JobStatusId], [Translation], [languageId]) VALUES (8, 4, N'تم الرفض ', 1)
INSERT INTO [Job].[JobStatusTranslation] ([Id], [JobStatusId], [Translation], [languageId]) VALUES (1001, 5, N'Closed', 2)
INSERT INTO [Job].[JobStatusTranslation] ([Id], [JobStatusId], [Translation], [languageId]) VALUES (1002, 5, N'أغلقت', 1)
SET IDENTITY_INSERT [Job].[JobStatusTranslation] OFF
SET IDENTITY_INSERT [Job].[JobStatusTranslation] ON
INSERT INTO [Job].[JobStatusTranslation] ([Id], [JobStatusId], [Translation], [languageId]) VALUES (3, 2, N'Posted', 2)
INSERT INTO [Job].[JobStatusTranslation] ([Id], [JobStatusId], [Translation], [languageId]) VALUES (4, 2, N'قيد التنفيذ', 1)
SET IDENTITY_INSERT [Job].[JobStatusTranslation] OFF
