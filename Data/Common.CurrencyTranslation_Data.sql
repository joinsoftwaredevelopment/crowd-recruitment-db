INSERT INTO [Common].[CurrencyTranslation] ([currencyId], [languageId], [name], [shortName]) VALUES (45, 1, N'???? ????', N'EGP')
INSERT INTO [Common].[CurrencyTranslation] ([currencyId], [languageId], [name], [shortName]) VALUES (45, 2, N'Egyptian pound', N'EGP')
INSERT INTO [Common].[CurrencyTranslation] ([currencyId], [languageId], [name], [shortName]) VALUES (122, 1, N'???? ?????', N'?.?')
INSERT INTO [Common].[CurrencyTranslation] ([currencyId], [languageId], [name], [shortName]) VALUES (122, 2, N'Saudi Arabian riyal', N'SAR')
