SET IDENTITY_INSERT [Employer].[FindingJobDifficultyLevelTranslation] ON
INSERT INTO [Employer].[FindingJobDifficultyLevelTranslation] ([Id], [FindingJobDifficultyLevelId], [Translation], [languageId]) VALUES (1, 1, N'Not Difficult', 2)
INSERT INTO [Employer].[FindingJobDifficultyLevelTranslation] ([Id], [FindingJobDifficultyLevelId], [Translation], [languageId]) VALUES (2, 1, N'ليس صعبا', 1)
INSERT INTO [Employer].[FindingJobDifficultyLevelTranslation] ([Id], [FindingJobDifficultyLevelId], [Translation], [languageId]) VALUES (3, 2, N'Medum', 2)
INSERT INTO [Employer].[FindingJobDifficultyLevelTranslation] ([Id], [FindingJobDifficultyLevelId], [Translation], [languageId]) VALUES (4, 2, N'متوسط ', 1)
INSERT INTO [Employer].[FindingJobDifficultyLevelTranslation] ([Id], [FindingJobDifficultyLevelId], [Translation], [languageId]) VALUES (5, 3, N'Very Difficult', 2)
INSERT INTO [Employer].[FindingJobDifficultyLevelTranslation] ([Id], [FindingJobDifficultyLevelId], [Translation], [languageId]) VALUES (6, 3, N'صعب جدا', 1)
SET IDENTITY_INSERT [Employer].[FindingJobDifficultyLevelTranslation] OFF
