SET IDENTITY_INSERT [Common].[Languages] ON
INSERT INTO [Common].[Languages] ([id], [name]) VALUES (1, N'Arabic')
INSERT INTO [Common].[Languages] ([id], [name]) VALUES (2, N'English')
SET IDENTITY_INSERT [Common].[Languages] OFF
