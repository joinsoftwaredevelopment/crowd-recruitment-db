SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[VwSourcers]
AS
SELECT        Common.Users.UUID AS uuid, Common.Users.FirstName AS firstName, Common.Users.LastName AS lastName, (FirstName + ' ' + LastName) as fullName, Common.Users.PhoneNumber AS phoneNumber, Common.Users.Email AS email, 
                         Common.Users.IsEmailVerified AS isEmailVerified, Common.Users.Password AS password, Common.Users.RoleId AS roleId, Common.Users.LanguageId AS languageId, CONVERT(nvarchar, Common.Users.CreatedDate, 103) AS createdDate, 
                         Common.Users.UpdatedDate AS updatedDate, Common.Users.Active AS active, Common.Users.Accepted AS accepted, Common.Users.MeetingDate, Common.Users.MeetingPeriodId AS meetingPeriodId, 
                         Common.Users.RegistrationStep AS registrationStep, Common.Users.InterviewerId AS interviewerId, Common.Users.meetingTimeId, Common.Users.timeZoneId, Common.Users.userStatusId, 
                         Sourcer.SourcerDetails.ExperienceTypeId AS experienceTypeId, Sourcer.SourcerDetails.JobTitle AS jobTitle, Sourcer.SourcerDetails.HasAccessToDifferentWebsite AS hasAccessToDifferentWebsite, 
                         Sourcer.SourcerDetails.IndustryId AS industryId, Sourcer.SourcerDetails.CvName AS cvName, Sourcer.SourcerDetails.LinkedInProfile AS linkedInProfile
FROM            Common.Users INNER JOIN
                         Sourcer.SourcerDetails ON Common.Users.Id = Sourcer.SourcerDetails.UserId
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Users (Common)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 215
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SourcerDetails (Sourcer)"
            Begin Extent = 
               Top = 6
               Left = 253
               Bottom = 136
               Right = 500
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'VwSourcers', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'VwSourcers', NULL, NULL
GO
