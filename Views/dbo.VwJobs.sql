SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE VIEW [dbo].[VwJobs]
AS
SELECT        Job.Job.Id AS jobId,Job.Job.UUID AS uuId, Job.Job.JobTitle AS jobTitle, Employer.EmploymentTypeTranslation.Translation AS employmentTypeName, 
              Employer.SeniorityLevelTranslation.Translation AS seniorityLevelName, 
              Job.Job.companiesNotToSourceFrom, Job.JobLocationTranslation.Translation AS jobLocation, 
			  Employer.EmployerDetails.CompanyName AS companyName, Job.Job.mustHaveQualification, 
			  Job.Job.niceToHaveQualification, 
              Job.Job.HiringNeeds, Employer.ExperienceLevelTranslation.Translation AS ExperienceLevelName , 
			  Common.Languages.id as languageId ,
			  Job.Job.JobDescription as description , (CompanyIndustryTranslation.Translation) as companyIndustry , 
			  Job.JobStatusTranslation.Translation as jobStatusName, Job.Job.requirements
FROM          Job.Job INNER JOIN
                         Employer.EmploymentType ON Job.Job.EmployerTypeId = Employer.EmploymentType.Id INNER JOIN
                         Employer.EmploymentTypeTranslation ON Employer.EmploymentType.Id = Employer.EmploymentTypeTranslation.EmploymentTypeId INNER JOIN
                         Common.Languages ON Common.Languages.id = Employer.EmploymentTypeTranslation.languageId INNER JOIN
                         Employer.SeniorityLevel ON Employer.SeniorityLevel.Id = Job.Job.SeniorityLevelId INNER JOIN
                         Employer.SeniorityLevelTranslation ON Employer.SeniorityLevelTranslation.SeniorityLevelId = Employer.SeniorityLevel.Id AND Employer.SeniorityLevelTranslation.languageId = Common.Languages.id INNER JOIN
                         Job.JobLocation ON Job.JobLocation.Id = Job.Job.JobLocationId INNER JOIN
                         Job.JobLocationTranslation ON Job.JobLocationTranslation.JobLocationId = Job.JobLocation.Id AND Job.JobLocationTranslation.languageId = Common.Languages.id INNER JOIN
                         Job.JobStatus ON Job.JobStatus.Id = Job.Job.JobStatusId INNER JOIN
                         Job.JobStatusTranslation ON Job.JobStatusTranslation.JobStatusId = Job.JobStatus.Id AND Job.JobStatusTranslation.languageId = Common.Languages.id INNER JOIN
						 Common.Users ON Common.Users.Id = Job.Job.EmployerId INNER JOIN
                         Employer.EmployerDetails ON Employer.EmployerDetails.UserId = Common.Users.Id INNER JOIN
                         Employer.ExperienceLevel ON Employer.ExperienceLevel.Id = Job.Job.ExperienceLevelId INNER JOIN
                         Employer.ExperienceLevelTranslation ON Employer.ExperienceLevelTranslation.ExperienceLevelId = Employer.ExperienceLevel.Id 
						 AND Employer.ExperienceLevelTranslation.languageId = Common.Languages.id
						 INNER JOIN Employer.CompanyIndustry ON Job.industryId = CompanyIndustry.Id
						 INNER JOIN Employer.CompanyIndustryTranslation ON CompanyIndustryTranslation.CompanyIndustryId = CompanyIndustry.Id
						 AND CompanyIndustryTranslation.languageId = JobLocationTranslation.languageId 
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Job (Job)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 298
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "EmploymentType (Employer)"
            Begin Extent = 
               Top = 6
               Left = 336
               Bottom = 119
               Right = 506
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "EmploymentTypeTranslation (Employer)"
            Begin Extent = 
               Top = 6
               Left = 544
               Bottom = 136
               Right = 737
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Languages (Common)"
            Begin Extent = 
               Top = 120
               Left = 336
               Bottom = 216
               Right = 506
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SeniorityLevel (Employer)"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 251
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SeniorityLevelTranslation (Employer)"
            Begin Extent = 
               Top = 138
               Left = 544
               Bottom = 268
               Right = 716
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobLocation (Job)"
            Begin Extent = 
               Top = 138
               L', 'SCHEMA', N'dbo', 'VIEW', N'VwJobs', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'eft = 754
               Bottom = 251
               Right = 924
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobLocationTranslation (Job)"
            Begin Extent = 
               Top = 216
               Left = 246
               Bottom = 346
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Users (Common)"
            Begin Extent = 
               Top = 252
               Left = 38
               Bottom = 382
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "EmployerDetails (Employer)"
            Begin Extent = 
               Top = 270
               Left = 454
               Bottom = 400
               Right = 631
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ExperienceLevel (Employer)"
            Begin Extent = 
               Top = 252
               Left = 754
               Bottom = 365
               Right = 924
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ExperienceLevelTranslation (Employer)"
            Begin Extent = 
               Top = 348
               Left = 246
               Bottom = 478
               Right = 428
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'VwJobs', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'VwJobs', NULL, NULL
GO
