SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*GROUP BY Job.Job.Id, Job.Job.UUID, Job.Job.JobStatusId, Job.Job.IsActive, Job.JobLocationTranslation.languageId, Sourcer.JobCandidateSubmission.SourcerId, EmployerId, Common.Users.UUID*/
CREATE VIEW [dbo].[VwJobSubmissionsSummary]
AS
SELECT DISTINCT Job.Job.Id AS jobId, Job.Job.UUID, Job.Job.JobStatusId, Job.Job.JobTitle, Job.Job.IsActive,
                             (SELECT        COUNT(*) AS Expr1
                               FROM            Sourcer.JobCandidateSubmission
                               WHERE        (JobId = Job.Job.Id)) AS jobCount, Job.JobLocationTranslation.languageId, Job.JobLocationTranslation.Translation AS jobLocationTranslation, Sourcer.JobCandidateSubmission.SourcerId, 
                         Job.Job.EmployerId, Common.Users.UUID AS employerUUId, CAST(Job.Job.JobDate AS date) AS jobDate, Job.Job.JobDescription AS description, 
                         Employer.CompanyIndustryTranslation.Translation AS companyIndustry , Common.Users.FirstName as employerFName , Common.Users.LastName as employerLName
FROM            Job.Job INNER JOIN
                         Job.JobStatus ON Job.JobStatus.Id = Job.Job.JobStatusId LEFT OUTER JOIN
                         Sourcer.JobCandidateSubmission ON Sourcer.JobCandidateSubmission.JobId = Job.Job.Id INNER JOIN
                         Job.JobLocation ON Job.JobLocation.Id = Job.Job.JobLocationId INNER JOIN
                         Job.JobLocationTranslation ON Job.JobLocation.Id = Job.JobLocationTranslation.JobLocationId INNER JOIN
                         Common.Users ON Common.Users.Id = Job.Job.EmployerId INNER JOIN
                         Employer.CompanyIndustry ON Job.Job.industryId = Employer.CompanyIndustry.Id INNER JOIN
                         Employer.CompanyIndustryTranslation ON Employer.CompanyIndustryTranslation.CompanyIndustryId = Employer.CompanyIndustry.Id AND 
                         Employer.CompanyIndustryTranslation.languageId = Job.JobLocationTranslation.languageId
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N' = 629
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CompanyIndustryTranslation (Employer)"
            Begin Extent = 
               Top = 516
               Left = 246
               Bottom = 646
               Right = 440
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'VwJobSubmissionsSummary', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Job (Job)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 298
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobStatus (Job)"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 251
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobCandidateSubmission_1"
            Begin Extent = 
               Top = 252
               Left = 38
               Bottom = 382
               Right = 284
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobLocation (Job)"
            Begin Extent = 
               Top = 138
               Left = 246
               Bottom = 251
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobLocationTranslation (Job)"
            Begin Extent = 
               Top = 384
               Left = 38
               Bottom = 514
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Users (Common)"
            Begin Extent = 
               Top = 384
               Left = 246
               Bottom = 514
               Right = 423
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CompanyIndustry (Employer)"
            Begin Extent = 
               Top = 516
               Left = 38
               Bottom', 'SCHEMA', N'dbo', 'VIEW', N'VwJobSubmissionsSummary', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1

GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'VwJobSubmissionsSummary', NULL, NULL
GO
