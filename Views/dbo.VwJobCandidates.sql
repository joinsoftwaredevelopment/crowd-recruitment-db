SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[VwJobCandidates]
AS
SELECT        ROW_NUMBER() OVER ( ORDER BY Job.Job.Id ) AS 'rowNumber' ,
         Job.Job.Id AS jobId, Job.Job.UUID AS jobUuId, Job.Job.JobTitle AS jobTitle, Cast(CAST(Job.Job.JobDate AS date ) as nvarchar) AS jobDate, Sourcer.JobCandidateSubmission.SourcerId AS sourcerId, 
                         Common.Users.FirstName + ' ' + Common.Users.LastName AS sourcerName, Sourcer.JobCandidateSubmission.UUID AS submissionUuId, Sourcer.JobCandidateSubmission.candidateName, 
                         Sourcer.JobCandidateSubmission.CV_Name AS cvName, Sourcer.JobCandidateSubmission.linkedInProfile, Common.CountriesTranslation.name AS countryName, 
                         Cast(CAST(Sourcer.JobCandidateSubmission.SubmissionDate AS date) as nvarchar) AS submissionDate, Common.CountriesTranslation.languageId
FROM            Sourcer.JobCandidateSubmission INNER JOIN
                         Job.Job ON Sourcer.JobCandidateSubmission.JobId = Job.Job.Id INNER JOIN
                         Common.Users ON Common.Users.Id = Sourcer.JobCandidateSubmission.SourcerId LEFT OUTER JOIN
                         Common.Countries ON Common.Countries.id = Sourcer.JobCandidateSubmission.locationId LEFT OUTER JOIN
                         Common.CountriesTranslation ON Common.Countries.id = Common.CountriesTranslation.countryId
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "JobCandidateSubmission (Sourcer)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 284
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Job (Job)"
            Begin Extent = 
               Top = 6
               Left = 322
               Bottom = 136
               Right = 582
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Users (Common)"
            Begin Extent = 
               Top = 6
               Left = 620
               Bottom = 136
               Right = 797
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Countries (Common)"
            Begin Extent = 
               Top = 6
               Left = 835
               Bottom = 136
               Right = 1005
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CountriesTranslation (Common)"
            Begin Extent = 
               Top = 6
               Left = 1043
               Bottom = 119
               Right = 1213
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter ', 'SCHEMA', N'dbo', 'VIEW', N'VwJobCandidates', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'= 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'VwJobCandidates', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'VwJobCandidates', NULL, NULL
GO
